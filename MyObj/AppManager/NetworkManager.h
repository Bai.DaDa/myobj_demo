//
//  NetworkManager.h
//  MyObj
//
//  Created by 巴图 on 2019/1/15.
//  Copyright © 2019 巴图. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <AFNetworking/AFNetworking.h>
#import "AFNetworking.h"
#import <AFNetworking/AFImageDownloader.h>

typedef NS_ENUM(NSInteger, RESPONSE_STATUS) {
    Status_Success             = 0,  //成功
    Status_Param_error         = 1,  //参数错误
    Status_Token_expired       = 2,  //token 过期
    Status_Token_error         = 3,  //token 错误
    Status_Refresh_token_error = 4,  //refresh_token 错误
    Status_Sign_error          = 5,  //sign 错误
    Status_System_maintenance  = 9,  //系统维护
    Status_System_error        = 10, //系统错误
};

extern NSInteger const kAFNetworkingTimeoutInterval;

NS_ASSUME_NONNULL_BEGIN

@interface NetworkManager : NSObject

+ (NetworkManager *)sharedInstance;

/*
 *  监听网络状态
 */
+ (void)NetworkReachability;

/**
 *  GET网络请求
 *
 *  @param URLString    请求的地址
 *  @param parameters   请求的参数
 *  @param success      请求成功回调
 *  @param failure      请求失败回调
 */
- (void)GETRequest:(nonnull NSString *)URLString
        parameters:(nullable id)parameters
           success:(nullable void (^)(id _Nullable responseObject))success
           failure:(nullable void (^)(NSError *error))failure;

/**
 *  POST网络请求
 *
 *  @param URLString    请求的地址
 *  @param parameters   请求的参数
 *  @param success      请求成功回调
 *  @param failure      请求失败回调
 */
- (void)POSTRequest:(nonnull NSString *)URLString
         parameters:(nullable id)parameters
            success:(nullable void (^)(id _Nullable responseObject))success
            failure:(nullable void (^)(NSError *error))failure;

/**
 *  DOWNLOAD IMAGE
 *
 *  @param URLString    请求的地址
 *  @param success      请求成功回调
 *  @param failure      请求失败回调
 */
- (void)downloadImage:(nonnull NSString *)URLString
              success:(nullable void (^)(id _Nullable imageData))success
              failure:(nullable void (^)(NSError *error))failure;

/**
 *  取消队列
 */
- (void)cancelAllDataTask;

@end

NS_ASSUME_NONNULL_END
