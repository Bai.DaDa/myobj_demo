//
//  NetworkManager.m
//  MyObj
//
//  Created by 巴图 on 2019/1/15.
//  Copyright © 2019 巴图. All rights reserved.
//

#import "NetworkManager.h"

#import "AppManager.h"

#import "NetworkRequestParameters.h"

#import "MBProgressHUD+MaxMethod.h"
#import "NSString+MaxMethod.h"

#import "BaseViewController.h"

NSInteger const kAFNetworkingTimeoutInterval = 15;

NSString *const kParamters_os_version  = @"os_version";
NSString *const kParamters_app_version = @"app_version";

NSString *const kParamters_uuid        = @"uuid";
NSString *const kParamters_user_id     = @"user_id";
NSString *const kParamters_token       = @"token";
NSString *const kParamters_timestamp   = @"timestamp";
NSString *const kParamters_sign        = @"sign";

NSString *const kParamters_device_os   = @"device_os";
NSString *const kDevice_os             = @"iOS";

NSString *const kResponse_data         = @"data";
NSString *const kResponse_status       = @"status";
NSString *const kResponse_message      = @"message";

NSString *const kNormalActionTitle     = @"OK";

@implementation NetworkManager

static NetworkManager *sharedInstance = nil;
static AFHTTPSessionManager *afnManager = nil;
static AFImageDownloader *afImageDownloader = nil;

+ (NetworkManager *)sharedInstance {
    static dispatch_once_t predicate;
    
    dispatch_once(&predicate, ^{
        sharedInstance = [[NetworkManager alloc] init];
        afnManager = [AFHTTPSessionManager manager];
        afnManager.requestSerializer = [AFHTTPRequestSerializer serializer];
        afnManager.requestSerializer.timeoutInterval = kAFNetworkingTimeoutInterval;
        //会出现返回的 responseObject 是 _NSInlineData 类型导致无法解析
//        afnManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        afnManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:
                                                                @"text/html",
                                                                @"text/xml",
                                                                @"text/json",
                                                                @"text/plain",
                                                                @"text/JavaScript",
                                                                @"application/json",
                                                                @"image/jpeg",
                                                                @"image/png",
                                                                @"application/octet-stream",
                                                                nil];
        
        afImageDownloader = [[AFImageDownloader alloc] init];
    });
    return sharedInstance;
}

+ (void)NetworkReachability {
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
            {
                NSLog(@"未知网络");
                [AppManager showAlertWithTitle:nil
                                       Message:@"未知网络"
                                   ActionTitle:kNormalActionTitle
                                       handler:^(UIAlertAction *action) {
                                           
                                       }];
            }
                break;
                
            case AFNetworkReachabilityStatusNotReachable:
            {
                NSLog(@"无网");
                [AppManager showAlertWithTitle:nil
                                       Message:@"无网"
                                   ActionTitle:kNormalActionTitle
                                       handler:^(UIAlertAction *action) {
                                           
                                       }];
            }
                break;
                
            case AFNetworkReachabilityStatusReachableViaWWAN:
            {
                NSLog(@"手机网络");
                [AppManager showAlertWithTitle:nil
                                       Message:@"手机网络"
                                   ActionTitle:kNormalActionTitle
                                       handler:^(UIAlertAction *action) {
                                           
                                       }];
            }
                break;
                
            case AFNetworkReachabilityStatusReachableViaWiFi:
            {
                NSLog(@"Wifi");
                //do nothing
//                [AppManager showAlertWithTitle:nil
//                                       Message:@"Wifi"
//                                   ActionTitle:kNormalActionTitle
//                                       handler:^(UIAlertAction *action) {
//                                           
//                                       }];
            }
                
            default:
                break;
        }
    }];
    [manager startMonitoring];
}

// get common paramters
- (NSMutableDictionary *)commonParamters {
    NSMutableDictionary *common = [NSMutableDictionary dictionary];
    
    [common setObject:[AppManager sharedInstance].system_version forKey:kParamters_os_version];
    [common setObject:[AppManager sharedInstance].app_version forKey:kParamters_app_version];
    
    NSMutableString *hash = [NSMutableString stringWithString:SALT];
    
    NSString *uuid = [AppManager sharedInstance].uuid;
    [common setObject:uuid forKey:kParamters_uuid];
    [hash appendString:uuid];
    
    NSString *user_id = [AppManager sharedInstance].user_id.stringValue;
    if (user_id.length > 0) {
        [common setObject:user_id forKey:kParamters_user_id];
        [hash appendString:user_id];
    }
    
    NSString *token = [AppManager sharedInstance].token;
    if (token.length > 0) {
        [common setObject:token forKey:kParamters_token];
        [hash appendString:token];
    }
    
    NSString *timestamp = [NSString stringWithFormat:@"%.0f", [[NSDate date] timeIntervalSince1970]];
    [hash appendString:timestamp];
    [common setObject:timestamp forKey:kParamters_timestamp];
    [common setObject:[hash MD5] forKey:kParamters_sign];
    [common setObject:kDevice_os forKey:kParamters_device_os];
    
    return common.mutableCopy;
}

- (NSDictionary *)getParamters:(NSDictionary *)paramter {
    NSMutableDictionary *commonParamters = [self commonParamters];
    if (paramter) {
        [commonParamters addEntriesFromDictionary:paramter];
    }
    
    NSLog(@"common Paramters = \n %@", commonParamters);
    return commonParamters;
}

- (NSString *)EncodingURLString:(NSString *)urlString {
    if (@available(iOS 9.0, *)) {
        urlString = [urlString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    }else {
        urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    return urlString;
}

//GET request
- (void)GETRequest:(NSString *)URLString
        parameters:(id)parameters
           success:(void (^)(id _Nullable))success
           failure:(void (^)(NSError * _Nonnull))failure {
    __weak __typeof(self) weakSelf = self;
    [afnManager GET:[self EncodingURLString:URLString]
         parameters:[self getParamters:parameters]
           progress:^(NSProgress * _Nonnull downloadProgress) {
               
           } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
               __strong __typeof(weakSelf) strongSelf = weakSelf;
               [MBProgressHUD hideHUD];
               NSNumber *status = responseObject[kResponse_status];
               if (status.integerValue == Status_Success) {
                   if (success) {
                       success(responseObject);
                   }
               }else {
                   [strongSelf showAlert:responseObject];
               }
           } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
               [MBProgressHUD hideHUD];
               NSLog(@"error ------ %@", error);
               if (error.code != -999) {
                   if (failure) {
                       failure(error);
                   }
               }else {
                   NSLog(@"被取消了");
               }
           }];
}

// POST request
- (void)POSTRequest:(NSString *)URLString
         parameters:(id)parameters
            success:(void (^)(id _Nullable))success
            failure:(void (^)(NSError * _Nonnull))failure {
    __weak __typeof(self) weakSelf = self;
    [afnManager POST:[self EncodingURLString:URLString]
          parameters:[self getParamters:parameters]
            progress:^(NSProgress * _Nonnull uploadProgress) {
                
            } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                __strong __typeof(weakSelf) strongSelf = weakSelf;
                [MBProgressHUD hideHUD];
                NSNumber *status = responseObject[kResponse_status];
                if (status.integerValue == Status_Success) {
                    if (success) {
                        success(responseObject);
                    }
                }else {
                    [strongSelf showAlert:responseObject];
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [MBProgressHUD hideHUD];
                NSLog(@"error ------ %@", error);
                if (error.code != -999) {
                    if (failure) {
                        failure(error);
                    }
                }else {
                    NSLog(@"被取消了");
                }
            }];
}

- (void)showAlert:(id _Nullable)responseObject {
    NSNumber *status = responseObject[kResponse_status];
    if (status.integerValue == Status_Token_error ||
              status.integerValue == Status_Token_expired ||
              status.integerValue == Status_Refresh_token_error) {
        [AppManager showAlertWithTitle:nil
                               Message:responseObject[kResponse_message]
                           ActionTitle:kNormalActionTitle
                               handler:^(UIAlertAction *action) {
                                   [[AppManager sharedInstance] logout];
                               }];
    }else {
        [AppManager showAlertWithTitle:nil
                               Message:responseObject[kResponse_message]
                           ActionTitle:kNormalActionTitle
                               handler:^(UIAlertAction *action) {

                               }];
    }
}

//download image
- (void)downloadImage:(NSString *)URLString
              success:(void (^)(id _Nullable))success
              failure:(void (^)(NSError * _Nonnull))failure {
    NSURL *url = [NSURL URLWithString:URLString];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
//    AFImageDownloadReceipt *receipt =
    [afImageDownloader downloadImageForURLRequest:request
                                          success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull responseObject) {
                                              if (responseObject) {
                                                  NSData *imageData = UIImagePNGRepresentation(responseObject);
                                                  if (success) {
                                                      success(imageData);
                                                  }
                                              }
                                          } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                                              //                                              [MBProgressHUD hideHUD];
                                              NSLog(@"error ------ %@", error);
                                              if (error.code != -999) {
                                                  if (failure) {
                                                      failure(error);
                                                  }
                                              }else {
                                                  NSLog(@"被取消了");
                                              }
                                          }];
}


//取消队列
- (void)cancelAllDataTask {
    NSArray *tasks = [NSArray arrayWithArray:[afnManager dataTasks]];
    for (NSURLSessionDataTask *task in tasks) {
        [task cancel];
    }
    
    NSArray *imageTasks = [NSArray arrayWithArray:[afImageDownloader.sessionManager dataTasks]];
    for (NSURLSessionDataTask *task in imageTasks) {
        [task cancel];
    }
}

@end
