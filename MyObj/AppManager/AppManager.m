//
//  AppManager.m
//  MyObj
//
//  Created by 巴图 on 2019/1/15.
//  Copyright © 2019 巴图. All rights reserved.
//

#import "AppManager.h"
#import "NetworkManager.h"
#import "NetworkRequestParameters.h"
#import <sys/utsname.h>
#import "NSUserDefaults+MaxMethod.h"

NSString *const gender_male        = @"Gender_Male";
NSString *const gender_female      = @"Gender_Female";

NSString *const ud_uuid            = @"UD_UUID";
NSString *const ud_token           = @"UD_TOKEN";
NSString *const ud_refresh_token   = @"UD_REFRESH_TOKEN";
NSString *const ud_expired         = @"UD_EXPIRED";
NSString *const ud_user_id         = @"UD_USER_ID";
NSString *const ud_user_name       = @"UD_USER_NAME";
NSString *const ud_user_gender     = @"UD_USER_GENDER";

NSString *const key_uuid           = @"uuid";
NSString *const key_token          = @"token";
NSString *const key_refresh_token  = @"refresh_token";
NSString *const key_expired        = @"expired";
NSString *const key_user_id        = @"account_id";
NSString *const key_user_name      = @"account_name";
NSString *const key_user_gender    = @"user_gender";

NSInteger const kRefreshTokenDay   = 3;

@implementation AppManager

static AppManager *sharedInstance = nil;

+ (AppManager *)sharedInstance {
    static dispatch_once_t predicate;
    
    dispatch_once(&predicate, ^{
        sharedInstance = [[AppManager alloc] init];
        [sharedInstance setup];
    });
    return sharedInstance;
}

- (void)setup {
    NSString *old_uuid = [NSUserDefaults getStringForKey:ud_uuid];
    if (old_uuid.length > 0) {
        self.uuid = old_uuid;
    }else {
        NSString *new_uuid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
        [NSUserDefaults saveString:new_uuid forKey:ud_uuid];
        
        self.uuid = new_uuid;
    }
    
    self.token = [NSUserDefaults getStringForKey:ud_token];
    self.refresh_token = [NSUserDefaults getStringForKey:ud_refresh_token];
    self.expired = [NSUserDefaults getNumberForKey:ud_expired];
    
    self.user_id = [NSUserDefaults getNumberForKey:ud_user_id];
    self.user_name = [NSUserDefaults getStringForKey:ud_user_name];
    
    [self checkUserGender:[NSUserDefaults getStringForKey:ud_user_gender]];
    
    self.system_version = [[UIDevice currentDevice] systemVersion];
    self.app_version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

- (void)checkUserGender:(NSString *)gender {
    if ([gender isEqualToString:gender_male]) {
        self.user_gender = MALE;
    }else if ([gender isEqualToString:gender_female]) {
        self.user_gender = FEMALE;
    }else {
        self.user_gender = UNKNOWN;
    }
}

//是否登录
+ (BOOL)isLoggedIn {
    return [[[[self class] sharedInstance] token] length] > 0 ? YES : NO;
}

- (void)saveLoginInformation:(NSDictionary *)info {
    self.uuid = info[key_uuid];
    self.token = info[key_token];
    self.refresh_token = info[key_refresh_token];
    self.expired = info[key_expired];
    self.user_id = info[key_user_id];
    self.user_name = info[key_user_name];
    [self checkUserGender:info[key_user_gender]];
    
    [NSUserDefaults saveString:info[key_uuid] forKey:ud_uuid];
    [NSUserDefaults saveString:info[key_token] forKey:ud_token];
    [NSUserDefaults saveString:info[key_refresh_token] forKey:ud_refresh_token];
    [NSUserDefaults saveNumber:info[key_expired] forKey:ud_expired];
    [NSUserDefaults saveNumber:info[key_user_id] forKey:ud_user_id];
    [NSUserDefaults saveNumber:info[key_user_name] forKey:ud_user_name];
    [self saveUserGender:info[key_user_gender]];
}

- (void)saveUserGender:(NSNumber *)gender {
    if (gender.integerValue == 1) {
        [NSUserDefaults saveString:gender_male forKey:ud_user_gender];
    }else if (gender.integerValue == 2) {
        [NSUserDefaults saveString:gender_female forKey:ud_user_gender];
    }
}

//退出登录
- (void)logout {
//    __weak __typeof(self) weakSelf = self;
    [[NetworkManager sharedInstance] POSTRequest:LOGOUT_API
                                      parameters:nil
                                         success:^(id  _Nullable responseObject) {
//                                             __strong __typeof(weakSelf) strongSelf = weakSelf;
//                                             [strongSelf clearLoginInformation];
                                             NSLog(@"退出登录");
                                         } failure:^(NSError * _Nonnull error) {
                                             NSLog(@"退出登录失败");
                                         }];
    [self clearLoginInformation];
    
    UIStoryboard *board = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController *vc = [board instantiateViewControllerWithIdentifier:@"LoginViewController"];
    UINavigationController *na = [[UINavigationController alloc] initWithRootViewController:vc];
    [UIApplication sharedApplication].keyWindow.rootViewController = na;
}

//清除登录信息
- (void)clearLoginInformation {
    self.uuid = nil;
    self.token = nil;
    self.refresh_token = nil;
    self.expired = nil;
    self.user_id = nil;
    self.user_name = nil;
    self.user_gender = UNKNOWN;
    
    [NSUserDefaults removeForKey:ud_uuid];
    [NSUserDefaults removeForKey:ud_token];
    [NSUserDefaults removeForKey:ud_refresh_token];
    [NSUserDefaults removeForKey:ud_expired];
    [NSUserDefaults removeForKey:ud_user_id];
    [NSUserDefaults removeForKey:ud_user_name];
    [NSUserDefaults removeForKey:ud_user_gender];
}

//刷新token
- (void)refreshToken {
    if (self.expired.integerValue > 0) {
        NSString *expiredStr = self.expired.stringValue;
        NSTimeInterval timeInterval = [[NSDate date] timeIntervalSince1970];
        NSInteger time = expiredStr.integerValue - (NSInteger)timeInterval;
        NSInteger day = time / 24 / 60 / 60;
        if (day > kRefreshTokenDay) return;
        
        __weak __typeof(self) weakSelf = self;
        [[NetworkManager sharedInstance] POSTRequest:REFRESH_TOKEN_API
                                          parameters:@{key_token : self.token,
                                                       key_refresh_token : self.refresh_token}
                                             success:^(id  _Nullable responseObject) {
                                                 __strong __typeof(weakSelf) strongSelf = weakSelf;
                                                 [strongSelf saveNewTokenInformation:responseObject[@"data"]];
                                                 NSLog(@"token 刷新成功");
                                             } failure:^(NSError * _Nonnull error) {
                                                 NSLog(@"token 刷新失败");
                                             }];
    }else {
        NSLog(@"没有token过期时间");
    }
}

//保存新的token信息
- (void)saveNewTokenInformation:(NSDictionary *)info {
    if (info) {
        self.token = info[key_token];
        self.refresh_token = info[key_refresh_token];
        self.expired = info[key_expired];
        
        [NSUserDefaults saveString:info[key_token] forKey:ud_token];
        [NSUserDefaults saveString:info[key_refresh_token] forKey:ud_refresh_token];
        [NSUserDefaults saveNumber:info[key_expired] forKey:ud_expired];
    }else {
        NSLog(@"token 返回错误");
    }
}

//获取设备型号
- (NSString *)iphoneType {
    //需要导入头文件：#import <sys/utsname.h>
    struct utsname systemInfo;
    uname(&systemInfo);

    NSString *device_code = [NSString stringWithCString:systemInfo.machine
                                               encoding:NSUTF8StringEncoding];
    
    static NSDictionary *deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        deviceNamesByCode = @{
                              //iPhone Simulator
                              @"i386" : @"iPhone Simulator",
                              @"x86_64" : @"iPhone Simulator",
                              
                              //iPod
                              @"iPod1,1" : @"iPod Touch 1G",
                              @"iPod2,1" : @"iPod Touch 2G",
                              @"iPod3,1" : @"iPod Touch 3G",
                              @"iPod4,1" : @"iPod Touch 4G",
                              @"iPod5,1" : @"iPod Touch 5G",
                              
                              //iPhone
                              @"iPhone1,1" : @"iPhone 2G",
                              @"iPhone1,2" : @"iPhone 3G",
                              @"iPhone2,1" : @"iPhone 3GS",
                              @"iPhone3,1" : @"iPhone 4",
                              @"iPhone3,2" : @"iPhone 4",
                              @"iPhone3,3" : @"iPhone 4",
                              @"iPhone4,1" : @"iPhone 4S",
                              @"iPhone5,1" : @"iPhone 5",
                              @"iPhone5,2" : @"iPhone 5",
                              @"iPhone5,3" : @"iPhone 5c",
                              @"iPhone5,4" : @"iPhone 5c",
                              @"iPhone6,1" : @"iPhone 5s",
                              @"iPhone6,2" : @"iPhone 5s",
                              @"iPhone7,1" : @"iPhone 6 Plus",
                              @"iPhone7,2" : @"iPhone 6",
                              @"iPhone8,1" : @"iPhone 6s",
                              @"iPhone8,2" : @"iPhone 6s Plus",
                              @"iPhone8,4" : @"iPhone SE",
                              @"iPhone9,1" : @"iPhone 7",
                              @"iPhone9,2" : @"iPhone 7 Plus",
                              @"iPhone10,1" : @"iPhone 8",
                              @"iPhone10,4" : @"iPhone 8",
                              @"iPhone10,2" : @"iPhone 8 Plus",
                              @"iPhone10,5" : @"iPhone 8 Plus",
                              @"iPhone10,3" : @"iPhone X",
                              @"iPhone10,6" : @"iPhone X",
                              @"iPhone11,2" : @"iPhone XS",
                              @"iPhone11,4" : @"iPhone XS Max",
                              @"iPhone11,6" : @"iPhone XS Max",
                              @"iPhone11,8" : @"iPhone XR",
                              
                              //iPad
                              @"iPad1,1" : @"iPad 1G",
                              @"iPad2,1" : @"iPad 2",
                              @"iPad2,2" : @"iPad 2",
                              @"iPad2,3" : @"iPad 2",
                              @"iPad2,4" : @"iPad 2",
                              @"iPad2,5" : @"iPad Mini 1G",
                              @"iPad2,6" : @"iPad Mini 1G",
                              @"iPad2,7" : @"iPad Mini 1G",
                              @"iPad3,1" : @"iPad 3",
                              @"iPad3,2" : @"iPad 3",
                              @"iPad3,3" : @"iPad 3",
                              @"iPad3,4" : @"iPad 4",
                              @"iPad3,5" : @"iPad 4",
                              @"iPad3,6" : @"iPad 4",
                              @"iPad4,1" : @"iPad Air",
                              @"iPad4,2" : @"iPad Air",
                              @"iPad4,3" : @"iPad Air",
                              @"iPad4,4" : @"iPad Mini 2G",
                              @"iPad4,5" : @"iPad Mini 2G",
                              @"iPad4,6" : @"iPad Mini 2G",
                              @"iPad4,7" : @"iPad Mini 3",
                              @"iPad4,8" : @"iPad Mini 3",
                              @"iPad4,9" : @"iPad Mini 3",
                              @"iPad5,1" : @"iPad Mini 4",
                              @"iPad5,2" : @"iPad Mini 4",
                              @"iPad5,3" : @"iPad Air 2",
                              @"iPad5,4" : @"iPad Air 2",
                              @"iPad6,3" : @"iPad Pro 9.7",
                              @"iPad6,4" : @"iPad Pro 9.7",
                              @"iPad6,7" : @"iPad Pro 12.9",
                              @"iPad6,8" : @"iPad Pro 12.9"
                              };
    }
    
    NSString *deviceName = [deviceNamesByCode objectForKey:device_code];
    
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        if ([device_code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }else if([device_code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }else if([device_code rangeOfString:@"iPhone"].location != NSNotFound) {
            deviceName = @"iPhone";
        }else {
            deviceName = @"Unknown";
        }
    }
    
    return deviceName;
}

//Alert
+ (void)showAlertWithTitle:(NSString *)title
                   Message:(NSString *)message
               ActionTitle:(NSString *)actionTitle
                   handler:(void (^)(UIAlertAction *))handler {
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:actionTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (handler) {
            handler(action);
        }
    }];
    [alertC addAction:action];
    
    UIViewController *controller = [UIApplication sharedApplication].keyWindow.rootViewController;
    while (controller.presentedViewController) {
        controller = controller.presentedViewController;
    }
    [controller presentViewController:alertC animated:YES completion:^{
        
    }];
}

@end
