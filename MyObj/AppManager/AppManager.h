//
//  AppManager.h
//  MyObj
//
//  Created by 巴图 on 2019/1/15.
//  Copyright © 2019 巴图. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, USERGENDER) {
    MALE = 0,
    FEMALE = 1,
    UNKNOWN = 2
};

//NS_ASSUME_NONNULL_BEGIN

@interface AppManager : NSObject

@property (nonatomic, copy) NSString *uuid;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *refresh_token;
@property (nonatomic, strong) NSNumber *expired;

@property (nonatomic, strong) NSNumber *user_id;
@property (nonatomic, copy) NSString *user_name;
@property (nonatomic, assign) USERGENDER user_gender;

//系统参数
@property (nonatomic, copy) NSString *system_version;
@property (nonatomic, copy) NSString *app_version;

+ (AppManager *)sharedInstance;
/*
 *  是否登录
 */
+ (BOOL)isLoggedIn;

/*
 *  保存登录信息
 *
 *  @param info    信息
 */
- (void)saveLoginInformation:(NSDictionary *)info;

/*
 *  退出登录
 */
- (void)logout;

/*
 *  刷新token
 */
- (void)refreshToken;

/*
 *  获取设备型号
 */
- (NSString *)iphoneType;

/**
 *  Alert
 *  Only OK action
 *
 *  @param title    标题
 *  @param message  信息
 */
+ (void)showAlertWithTitle:(nullable NSString *)title
                   Message:(nullable NSString *)message
               ActionTitle:(nonnull NSString *)actionTitle
                   handler:(nullable void (^)(UIAlertAction *action))handler;

@end

//NS_ASSUME_NONNULL_END
