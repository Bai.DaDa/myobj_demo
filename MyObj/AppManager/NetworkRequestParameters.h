//
//  NetworkRequestParameters.h
//  MyObj
//
//  Created by 巴图 on 2019/1/15.
//  Copyright © 2019 巴图. All rights reserved.
//
//
//#ifndef NetworkRequestParameters_h
//#define NetworkRequestParameters_h
//
//
//#endif /* NetworkRequestParameters_h */


#define URL_WITH_PATH(PATH)            [NSString stringWithFormat:@"%@%@", BASE_URL, PATH]

//MyObj
#ifdef MyObj

#define SALT                           @"winter_is_coming"
#define BASE_URL                       @"http://amulet.bsbj.net/api/"

//url

#endif



//MyObj_dev
#ifdef MyObj_dev

#define SALT                           @"winter_is_coming"
#define BASE_URL                       @"http://amulet.bsbj.net/api/"

//url

#endif



////japan pro
//#ifdef ProJAN
//
//#define SALT                           @"SALT"
//#define BASE_URL                       @""
//
////url
//
//#endif


//api url
#define LOGIN_API                      URL_WITH_PATH(@"account/login")
#define LOGOUT_API                     URL_WITH_PATH(@"account/logout")
#define REFRESH_TOKEN_API              URL_WITH_PATH(@"account/refresh-token")


