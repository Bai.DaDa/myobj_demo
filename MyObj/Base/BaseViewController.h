//
//  BaseViewController.h
//  MyObj
//
//  Created by 巴图 on 2019/1/17.
//  Copyright © 2019 巴图. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController

- (void)setNavigationBackGroundColorWithColor:(nullable UIColor *)color;

- (void)setLeftBarButtonItemWithText:(nonnull NSString *)text;
- (void)setLeftBarButtonItemWithImage:(nonnull NSString *)image;
- (void)setLeftBarButtonItemWithSystemItem:(UIBarButtonSystemItem)systemItem;


- (void)setRightBarButtonItemWithText:(nonnull NSString *)text;
- (void)setRightBarButtonItemWithImage:(nonnull NSString *)image;
- (void)setRightBarButtonItemWithSystemItem:(UIBarButtonSystemItem)systemItem;

- (void)leftBarButtonItemAction;
- (void)rightBarButtonItemAction;

@end

NS_ASSUME_NONNULL_END
