//
//  BaseViewController.m
//  MyObj
//
//  Created by 巴图 on 2019/1/17.
//  Copyright © 2019 巴图. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

//导航栏背景色
- (void)setNavigationBackGroundColorWithColor:(UIColor *)color {
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.barTintColor = color;
}


//导航栏左按钮
- (void)setLeftBarButtonItemWithText:(NSString *)text {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:text style:UIBarButtonItemStyleDone target:self action:@selector(leftBarButtonItemAction)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
}

- (void)setLeftBarButtonItemWithImage:(NSString *)image {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:image] style:UIBarButtonItemStyleDone target:self action:@selector(leftBarButtonItemAction)];
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
}

- (void)setLeftBarButtonItemWithSystemItem:(UIBarButtonSystemItem)systemItem {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:systemItem target:self action:@selector(leftBarButtonItemAction)];
}

//导航栏右按钮
- (void)setRightBarButtonItemWithText:(NSString *)text {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:text style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemAction)];
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor whiteColor];
}

- (void)setRightBarButtonItemWithImage:(NSString *)image {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:image] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemAction)];
}

- (void)setRightBarButtonItemWithSystemItem:(UIBarButtonSystemItem)systemItem {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:systemItem target:self action:@selector(rightBarButtonItemAction)];
}

//导航栏左按钮 Action
- (void)leftBarButtonItemAction {
    
}

//导航栏右按钮 Action
- (void)rightBarButtonItemAction {
    
}


- (void)dealloc {
    NSLog(@"%@ ------ dealloc", [self class]);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
