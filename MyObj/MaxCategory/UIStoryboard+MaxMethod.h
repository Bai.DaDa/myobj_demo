//
//  UIStoryboard+MaxMethod.h
//  Bricks
//
//  Created by 少年 on 2018/11/8.
//  Copyright © 2018年 bsbj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIStoryboard (MaxMethod)

+ (UIStoryboard *)loadWithName:(NSString *)name;
+ (UIViewController *)loadViewController:(Class)viewController;

@end
