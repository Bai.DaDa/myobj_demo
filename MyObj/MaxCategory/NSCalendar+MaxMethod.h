//
//  NSCalendar+MaxMethod.h
//  Amulet
//
//  Created by 少年 on 2018/11/26.
//  Copyright © 2018年 bsbj. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSCalendar (MaxMethod)

+ (NSInteger)getYearWithDate:(NSDate *)date;
+ (NSInteger)getMonthWithDate:(NSDate *)date;
+ (NSInteger)getDayWithDate:(NSDate *)date;
+ (NSInteger)getWeekWithDate:(NSDate *)date;
+ (NSString *)getWeekdayStrWithDate:(NSDate *)date;

@end
