//
//  UITableView+MaxMethond.h
//  Bricks
//
//  Created by 少年 on 2018/11/8.
//  Copyright © 2018年 bsbj. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (MaxMethond)

- (void)registerNib:(NSString *)nibName;
- (void)registerClass:(NSString *)className;

@end
