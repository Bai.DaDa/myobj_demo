//
//  UIStoryboard+MaxMethod.m
//  Bricks
//
//  Created by 少年 on 2018/11/8.
//  Copyright © 2018年 bsbj. All rights reserved.
//

#import "UIStoryboard+MaxMethod.h"

@implementation UIStoryboard (MaxMethod)

+ (UIStoryboard *)loadWithName:(NSString *)name {
    return [UIStoryboard storyboardWithName:name bundle:[NSBundle mainBundle]];
}

+ (UIViewController *)loadViewController:(Class)viewController {
    return [[UIStoryboard loadWithName:@"Main"] instantiateViewControllerWithIdentifier:NSStringFromClass(viewController)];
}


@end
