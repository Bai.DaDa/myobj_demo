//
//  UIButton+MaxMethod.h
//  Bricks
//
//  Created by 少年 on 2018/11/13.
//  Copyright © 2018年 bsbj. All rights reserved.
//

#import <UIKit/UIKit.h>

// 定义一个枚举（包含了四种类型的button）
typedef NS_ENUM(NSInteger, ButtonImageEdgeInsetsStyle) {
    /**
     *  image在上，label在下
     */
    ButtonImageEdgeInsetsStyleTop,
    /**
     *  image在左，label在右
     */
    ButtonImageEdgeInsetsStyleLeft,
    /**
     *  image在下，label在上
     */
    ButtonImageEdgeInsetsStyleBottom,
    /**
     *  image在右，label在左
     */
    ButtonImageEdgeInsetsStyleRight
};


@interface UIButton (MaxMethod)

/**
 * 设置button的titleLabel和imageView的布局样式，及间距
 *
 * @param style titleLabel和imageView的布局样式
 * @param space titleLabel和imageView的间距
 */
- (void)layoutButtonWithEdgeInsetsStyle:(ButtonImageEdgeInsetsStyle)style imageTitleSpace:(CGFloat)space;


/**
 设置角标的个数（右上角）
 @param badgeValue <#badgeValue description#>
 */
- (void)setBadgeValue:(NSInteger)badgeValue;

@end
