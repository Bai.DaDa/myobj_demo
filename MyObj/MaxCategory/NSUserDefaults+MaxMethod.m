//
//  NSUserDefaults+MaxMethod.m
//  NewLampSurvey
//
//  Created by 少年 on 2018/8/17.
//  Copyright © 2018年 少年. All rights reserved.
//

#import "NSUserDefaults+MaxMethod.h"

@implementation NSUserDefaults (MaxMethod)

+ (BOOL)saveObject:(id)value forKey:(NSString *)key {
    if (key) {
        [[self standardUserDefaults] setObject:value forKey:key];
        [[self standardUserDefaults] synchronize];
        return YES;
    }else {
        return NO;
    }
}

#pragma mark - save method
+ (BOOL)saveString:(NSString *)value forKey:(NSString *)key {
    return [self saveObject:value forKey:key];
}

+ (BOOL)saveNumber:(NSNumber *)value forKey:(NSString *)key {
    return [self saveObject:value forKey:key];
}

+ (BOOL)saveBool:(BOOL)value forKey:(NSString *)key {
    return [self saveObject:[NSNumber numberWithBool:value] forKey:key];
}

+ (BOOL)saveInteger:(NSInteger)value forKey:(NSString *)key {
    return [self saveObject:[NSNumber numberWithInteger:value] forKey:key];
}

+ (BOOL)saveFloat:(float)value forKey:(NSString *)key {
    return [self saveObject:[NSNumber numberWithFloat:value] forKey:key];
}

+ (BOOL)saveLong:(long)value forKey:(NSString *)key {
    return [self saveObject:[NSNumber numberWithLong:value] forKey:key];
}


#pragma mark - get NSString
+ (NSString *)getStringForKey:(NSString *)key defaultValue:(NSString *)defaultValue {
    if (!key) {
        return defaultValue;
    }
    NSString *string = [self getObjectForKey:key];
    if (string) {
        return string;
    }else {
        return defaultValue;
    }
}

+ (NSString *)getStringForKey:(NSString *)key {
    return [self getStringForKey:key defaultValue:nil];
}

#pragma mark - get NSNumber
+ (NSNumber *)getNumberForKey:(NSString *)key defaultValue:(NSNumber *)defaultValue {
    if (!key) {
        return defaultValue;
    }
    NSNumber *number = [self getObjectForKey:key];
    if (number) {
        return number;
    }else {
        return defaultValue;
    }
}

+ (NSNumber *)getNumberForKey:(NSString *)key {
    NSNumber *number = [self getObjectForKey:key];
    if (key) {
        return number;
    }else {
        return nil;
    }
}

+ (BOOL)getBoolForKey:(NSString *)key defaultValue:(BOOL)defaultValue {
    NSNumber *num = [self getNumberForKey:key];
    if (num) {
        return [num boolValue];
    }else {
        return defaultValue;
    }
}
+ (BOOL)getBoolForKey:(NSString *)key {
    return [self getBoolForKey:key defaultValue:NO];
}

+ (NSInteger)getIntegerForKey:(NSString *)key defaultValue:(NSInteger)defaultValue {
    NSNumber *num = [self getNumberForKey:key];
    if (num) {
        return [num integerValue];
    } else {
        return defaultValue;
    }
}

+ (NSInteger)getIntegerForKey:(NSString *)key {
    return [self getIntegerForKey:key defaultValue:0];
}

+ (long)getLongForKey:(NSString *)key defaultValue:(long)defaultValue {
    NSNumber *num = [self getNumberForKey:key];
    if (num) {
        return [num longValue];
    } else {
        return defaultValue;
    }
}
+ (long)getLongForKey:(NSString *)key {
    return [self getLongForKey:key defaultValue:0];
}

+ (float)getFlaotForKey:(NSString *)key defaultValue:(float)defaultValue {
    NSNumber *num = [self getNumberForKey:key];
    if (num) {
        return [num floatValue];
    } else {
        return defaultValue;
    }
}
+ (float)getFlaotForKey:(NSString*)key {
    return [self getFlaotForKey:key defaultValue:0];
}

+ (id)getObjectForKey:(NSString *)key {
    if (key) {
        return [[self standardUserDefaults] objectForKey:key];
    }else {
        return nil;
    }
}

#pragma mark - remove method
+ (BOOL)removeForKey:(NSString *)key {
    if (key) {
        [[self standardUserDefaults] removeObjectForKey:key];
        return YES;
    }else {
        return NO;
    }
}

@end
