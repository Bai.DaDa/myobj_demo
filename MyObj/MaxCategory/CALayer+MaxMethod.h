//
//  CALayer+MaxMethod.h
//  Amulet
//
//  Created by 少年 on 2018/11/20.
//  Copyright © 2018年 bsbj. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@class UIColor;
//@class CGColorRef;

@interface CALayer (MaxMethod)
- (void)setBorderColorFromUIColor:(UIColor *)color;
@end
