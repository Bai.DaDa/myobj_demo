//
//  NSDate+MaxMethod.h
//  NewLampSurvey
//
//  Created by 少年 on 2018/8/16.
//  Copyright © 2018年 少年. All rights reserved.
//

#import <Foundation/Foundation.h>

#define tokyo_timezone      @"Asia/Tokyo"
#define beijing_timezone    @"Beijing/Tokyo"
#define shanghai_timezone   @"Shanghai/Tokyo"

@interface NSDate (MaxMethod)

+ (NSTimeInterval)timestamp;

//YYYY-MM-dd HH:mm:ss
+ (NSDateFormatter *)getDateFormatter;
//YYYYMMddHHmmss
+ (NSString *)getNowTimeString;
//YYYY-MM-dd HH:mm:ss
+ (NSString *)getNowDateString;
//YYYYMMdd
+ (NSString *)getNowDateDayString;
//timestamp
+ (NSString *)getNowTimeTimestamp;
+ (BOOL)judegeTodayIsLoginTimeOutWithLastLoginDate:(NSString *)lastDate;


- (NSString *)dateStringWithDateFormat:(NSString *)dateFormat;
+ (NSString *)getDateStringWithDate:(NSDate *)date;
+ (NSDate *)getDateWithDateString:(NSString *)string;



- (NSString *)getTokyoNowDateTime;

/**
 *  date          : 2018-12-29 15:00:00 +0000 (🌰)
 *  dateFormatter : @"yyyy-MM-dd HH:mm:ss"(🌰)
 *  timeZone      : @"Asia/Beijing"(🌰)
 */
+ (NSString *)getTimeStringWithDate:(NSDate *)date DateFormatter:(NSString *)dateFormatter TimeZone:(NSString *)timeZone;
/**
 *  dateStr       : @"2018-01-01 09:00:00"(🌰)
 *  dateFormatter : @"yyyy-MM-dd HH:mm:ss"(🌰)
 *  timeZone      : @"Asia/Beijing"(🌰)
 */
+ (NSDate *)getTimeDateWithDateString:(NSString *)dateStr DateFormatter:(NSString *)dateFormatter TimeZone:(NSString *)timeZone;
@end
