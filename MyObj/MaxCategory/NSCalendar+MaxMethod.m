//
//  NSCalendar+MaxMethod.m
//  Amulet
//
//  Created by 少年 on 2018/11/26.
//  Copyright © 2018年 bsbj. All rights reserved.
//

#import "NSCalendar+MaxMethod.h"

@implementation NSCalendar (MaxMethod)

+ (NSInteger)getYearWithDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    //year
    return [calendar component:NSCalendarUnitYear fromDate:date];
}

+ (NSInteger)getMonthWithDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    //month
    return [calendar component:NSCalendarUnitMonth fromDate:date];
}

+ (NSInteger)getDayWithDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    //day
    return [calendar component:NSCalendarUnitDay fromDate:date];
}

+ (NSInteger)getWeekWithDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    //week
    return [calendar component:NSCalendarUnitWeekday fromDate:date];
}

+ (NSString *)getWeekdayStrWithDate:(NSDate *)date {
    NSString *week = @"";
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    //week
    switch ([calendar component:NSCalendarUnitWeekday fromDate:date]) {
        case 1:
            week = @"日曜日";
            break;
            
        case 2:
            week = @"月曜日";
            break;
            
        case 3:
            week = @"火曜日";
            break;
            
        case 4:
            week = @"水曜日";
            break;
            
        case 5:
            week = @"木曜日";
            break;
            
        case 6:
            week = @"金曜日";
            break;
            
        case 7:
            week = @"土曜日";
            break;
            
        default:
            week = @"日曜日";
            break;
    }
    return week;
}

@end
