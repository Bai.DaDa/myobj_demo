//
//  UITextView+MaxMethod.h
//  NewLampSurvey
//
//  Created by 少年 on 2018/10/12.
//  Copyright © 2018年 少年. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (MaxMethod)

@property (nonatomic, copy) NSString *placeHolder;

@end
