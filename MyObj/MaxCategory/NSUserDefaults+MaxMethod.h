//
//  NSUserDefaults+MaxMethod.h
//  NewLampSurvey
//
//  Created by 少年 on 2018/8/17.
//  Copyright © 2018年 少年. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (MaxMethod)

//save method
+ (BOOL)saveObject:(id)value forKey:(NSString *)key;

+ (BOOL)saveString:(NSString *)value forKey:(NSString *)key;
+ (BOOL)saveNumber:(NSNumber *)value forKey:(NSString *)key;
+ (BOOL)saveBool:(BOOL)value forKey:(NSString *)key;
+ (BOOL)saveInteger:(NSInteger)value forKey:(NSString *)key;
+ (BOOL)saveFloat:(float)value forKey:(NSString *)key;
+ (BOOL)saveLong:(long)value forKey:(NSString *)key;

//get method
+ (NSString *)getStringForKey:(NSString *)key defaultValue:(NSString *)defaultValue;
+ (NSString *)getStringForKey:(NSString *)key;

+ (NSNumber *)getNumberForKey:(NSString *)key defaultValue:(NSNumber *)defaultValue;
+ (NSNumber *)getNumberForKey:(NSString *)key;

+ (BOOL)getBoolForKey:(NSString *)key defaultValue:(BOOL)defaultValue;
+ (BOOL)getBoolForKey:(NSString *)key;

+ (NSInteger)getIntegerForKey:(NSString *)key defaultValue:(NSInteger)defaultValue;
+ (NSInteger)getIntegerForKey:(NSString *)key;

+ (long)getLongForKey:(NSString *)key defaultValue:(long)defaultValue;
+ (long)getLongForKey:(NSString *)key;

+ (float)getFlaotForKey:(NSString *)key defaultValue:(float)defaultValue;
+ (float)getFlaotForKey:(NSString*)key;

+ (id)getObjectForKey:(NSString *)key;

//remove method
+ (BOOL)removeForKey:(NSString *)key;

@end
