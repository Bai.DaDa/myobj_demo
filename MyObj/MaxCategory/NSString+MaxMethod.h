//
//  NSString+MaxMethod.h
//  NewLampSurvey
//
//  Created by 少年 on 2018/8/16.
//  Copyright © 2018年 少年. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (MaxMethod)

+ (CGFloat)getWidthWithText:(NSString *)text textFont:(UIFont *)textFont;
+ (CGSize)getSizeWithText:(NSString *)text needWidth:(CGFloat)needWidth textFont:(UIFont *)textFont;

+ (NSString *)stringByArray:(NSArray<NSString *> *)array andSeparatedSign:(NSString *)separatedSign;

+ (NSString *)getAfterName:(NSString *)name withSymbol:(NSString *)symbol;
+ (NSString *)getFrontName:(NSString *)name withSymbol:(NSString *)symbol;

- (NSString *)MD5;

- (UIImage *)createRRcode;
- (UIImage *)getHDRcodeWithScale:(CGFloat)scale;
- (UIImage *)drawImage;

- (BOOL)isPureFloat;

- (NSDate *)convertDateWithFormatter:(NSString *)format;
- (NSDate *)convertDateWithFormatter:(NSString *)format timeZone:(NSString *)timeZone;

@end
