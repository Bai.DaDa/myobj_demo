//
//  UIColor+MaxMethod.m
//  NewLampSurvey
//
//  Created by 少年 on 2018/8/16.
//  Copyright © 2018年 少年. All rights reserved.
//

#import "UIColor+MaxMethod.h"

@implementation UIColor (MaxMethod)

+ (UIColor *)colorWithHex:(int)hex {
    return [self colorWithHex:hex alpha:1.0];
}

+ (UIColor *)colorWithHex:(int)hex alpha:(CGFloat)alpha {
    CGFloat red = ((hex & 0xff0000) >> 16) / 255.0;
    CGFloat green = ((hex & 0xff00) >> 8) / 255.0;
    CGFloat blue = (hex & 0xff) / 255.0;
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

@end
