//
//  CALayer+MaxMethod.m
//  Amulet
//
//  Created by 少年 on 2018/11/20.
//  Copyright © 2018年 bsbj. All rights reserved.
//

#import "CALayer+MaxMethod.h"

@implementation CALayer (MaxMethod)

- (void)setBorderColorFromUIColor:(UIColor *)color {
    self.borderColor = color.CGColor;
}

@end
