//
//  UITableView+MaxMethond.m
//  Bricks
//
//  Created by 少年 on 2018/11/8.
//  Copyright © 2018年 bsbj. All rights reserved.
//

#import "UITableView+MaxMethond.h"

@implementation UITableView (MaxMethond)

- (void)registerNib:(NSString *)nibName {
    UINib *nib = [UINib nibWithNibName:nibName bundle:nil];
    [self registerNib:nib forCellReuseIdentifier:nibName];
}

- (void)registerClass:(NSString *)className {
    [self registerClass:NSClassFromString(className) forCellReuseIdentifier:className];
}

@end
