//
//  UIColor+MaxMethod.h
//  NewLampSurvey
//
//  Created by 少年 on 2018/8/16.
//  Copyright © 2018年 少年. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MaxMethod)

+ (UIColor *)colorWithHex:(int)hex;
+ (UIColor *)colorWithHex:(int)hex alpha:(CGFloat) alpha;

@end
