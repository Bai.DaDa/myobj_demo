//
//  UITextView+MaxMethod.m
//  NewLampSurvey
//
//  Created by 少年 on 2018/10/12.
//  Copyright © 2018年 少年. All rights reserved.
//

#import "UITextView+MaxMethod.h"
#import <objc/runtime.h>

static NSString *Key_placeHolder = @"Key_placeHolder";
static UILabel *placeHolderLabel = nil;

@implementation UITextView (MaxMethod)

- (void)addPlaceHolderLabel:(NSString *)placeHolder {
    if (!placeHolderLabel) {
        placeHolderLabel = [[UILabel alloc] init];
        placeHolderLabel.numberOfLines = 0;
        placeHolderLabel.textColor = [UIColor lightGrayColor];
        [placeHolderLabel sizeToFit];
        [self addSubview:placeHolderLabel];
        placeHolderLabel.font = self.font;
        [self setValue:placeHolderLabel forKey:@"_placeholderLabel"];
        
//        [self addObserver:self forKeyPath:@"placeHolder" options:NSKeyValueObservingOptionNew context:nil];
    }
    placeHolderLabel.text = placeHolder;
}

- (void)setPlaceHolder:(NSString *)placeHolder {
//    objc_setAssociatedObject(self, &Key_placeHolder, placeHolder, OBJC_ASSOCIATION_COPY);
    objc_setAssociatedObject(self, @selector(placeHolder), placeHolder, OBJC_ASSOCIATION_COPY);
    [self addPlaceHolderLabel:placeHolder];
}

- (NSString *)placeHolder {
//    return objc_getAssociatedObject(self, &Key_placeHolder);
    return objc_getAssociatedObject(self, _cmd);
}

//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
//    NSLog(@"keyPath ---- %@", keyPath);
//    NSLog(@"object ---- %@", object);
//    NSLog(@"change ---- %@", change);
//    NSLog(@"context ---- %@", context);
//    NSLog(@"\n\n\n\n");
//}


@end
