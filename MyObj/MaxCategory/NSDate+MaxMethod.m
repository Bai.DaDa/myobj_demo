//
//  NSDate+MaxMethod.m
//  NewLampSurvey
//
//  Created by 少年 on 2018/8/16.
//  Copyright © 2018年 少年. All rights reserved.
//

#import "NSDate+MaxMethod.h"

#define TOKYO     @"Asia/Tokyo"
#define BEIJING   @"Asia/Beijing"

@implementation NSDate (MaxMethod)

+ (NSTimeInterval)timestamp {
    return [[self date] timeIntervalSince1970];
}

+ (NSDateFormatter *)getDateFormatter {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateStyle:NSDateFormatterMediumStyle];
//    [formatter setTimeStyle:NSDateFormatterFullStyle];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    formatter.timeZone = [NSTimeZone timeZoneWithName:TOKYO];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    return formatter;
}

+ (NSString *)getNowTimeString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    formatter.dateStyle = NSDateFormatterMediumStyle;
//    formatter.timeStyle = NSDateFormatterShortStyle;
    formatter.dateFormat = @"yyyyMMddHHmmss";
    formatter.timeZone = [NSTimeZone timeZoneWithName:TOKYO];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    NSString *timeStr = [formatter stringFromDate:[NSDate date]];
    return timeStr;
}

+ (NSString *)getNowDateString {
    NSDateFormatter *formatter = [self getDateFormatter];
    NSDate *datenow = [NSDate date];
    NSString *dateTime = [formatter stringFromDate:datenow];
    return dateTime;
}

+ (NSString *)getNowDateDayString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    formatter.dateStyle = NSDateFormatterMediumStyle;
//    formatter.timeStyle = NSDateFormatterShortStyle;
    formatter.dateFormat = @"yyyyMMdd";
    formatter.timeZone = [NSTimeZone timeZoneWithName:TOKYO];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    NSString *timeStr = [formatter stringFromDate:[NSDate date]];
    return timeStr;
}

+ (NSString *)getNowTimeTimestamp {
    NSDateFormatter *formatter = [self getDateFormatter];
    NSDate *datenow = [NSDate date];
    NSString *timeSp = [NSString stringWithFormat:@"%lf", [datenow timeIntervalSince1970]];
    return timeSp;
}

+ (BOOL)judegeTodayIsLoginTimeOutWithLastLoginDate:(NSString *)lastDate {
    if ([lastDate isEqualToString:[self getNowDateDayString]]) {
        return YES;
    }else {
        return NO;
    }
}

- (NSString *)getTokyoNowDateTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    formatter.dateStyle = NSDateFormatterMediumStyle;
//    formatter.timeStyle = NSDateFormatterShortStyle;
    formatter.dateFormat = @"yyyyMMddHHmmss";
    formatter.timeZone = [NSTimeZone timeZoneWithName:TOKYO];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    NSString *timeStr = [formatter stringFromDate:[NSDate date]];
    return timeStr;
}

- (NSString *)getDateString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    formatter.timeZone = [NSTimeZone timeZoneWithName:TOKYO];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    NSString *timeStr = [formatter stringFromDate:[NSDate date]];
    return timeStr;
}


- (NSString *)dateStringWithDateFormat:(NSString *)dateFormat {
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = dateFormat;
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"Asia/Tokyo"];
    return [formatter stringFromDate:self];
}

+ (NSString *)getDateStringWithDate:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"Asia/Tokyo"];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    NSString *timeStr = [formatter stringFromDate:date];
    return timeStr;
}

+ (NSDate *)getDateWithDateString:(NSString *)string {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMdd";
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"Asia/Tokyo"];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    NSDate *timeDate = [formatter dateFromString:string];
    return timeDate;
}

+ (NSString *)getTimeStringWithDate:(NSDate *)date DateFormatter:(NSString *)dateFormatter TimeZone:(NSString *)timeZone {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = dateFormatter;
    formatter.timeZone = [NSTimeZone timeZoneWithName:timeZone];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    NSString *timeStr = [formatter stringFromDate:date];
    return timeStr;
}

+ (NSDate *)getTimeDateWithDateString:(NSString *)dateStr DateFormatter:(NSString *)dateFormatter TimeZone:(NSString *)timeZone {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = dateFormatter;
    formatter.timeZone = [NSTimeZone timeZoneWithName:timeZone];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    NSDate *date = [formatter dateFromString:dateStr];
    return date;
}

@end
